﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameState
{
    InGame,
    Pause
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameState currentGameState;

    public GameObject inventoryScreen;
    public GameObject gameHUD;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentGameState = GameState.InGame;
    }

    // Update is called once per frame
    void Update()
    {
        HandleGameState();
    }

    void HandleGameState()
    {
        switch (currentGameState)
        {
            case GameState.InGame:
                {
                    Time.timeScale = 1f;
                    gameHUD.SetActive(true);
                    inventoryScreen.SetActive(false);
                }
                break;
            case GameState.Pause:
                {
                    Time.timeScale = 0f;
                    gameHUD.SetActive(false);
                    inventoryScreen.SetActive(true);
                }
                break;
        }
    }
}
