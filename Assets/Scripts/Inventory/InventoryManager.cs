﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    [Header("Inventory Information")]
    public PlayerInventory characterInventory;
    [SerializeField] private GameObject blankInventorySlot;
    [SerializeField] private GameObject inventoryPanel;
    [SerializeField] private Text itemNameText;
    [SerializeField] private Text itemDescriptionText;
    [SerializeField] private GameObject useButton;
    public InventoryItem currentItem;
    public InventoryItem itemToAdd;
    
    private void Awake()
    {
        useButton.GetComponent<Button>().onClick.AddListener(UseButtonPressed);
    }

    // Start is called before the first frame update
    void Start()
    {
        SetInfoFirstTime("", "", false);
        MakeInventorySlots();
    }

    void SetInfoFirstTime(string newName, string newDescriptionString, bool isButtonUsable)
    {
        itemNameText.text = newName;
        itemDescriptionText.text = newDescriptionString;
        useButton.SetActive(isButtonUsable);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void MakeInventorySlots()
    {
        if (characterInventory)
        {
            for (int i = 0; i < characterInventory.playerInventory.Count; i++)
            {
                CreateSlot(characterInventory.playerInventory[i]);
            }
        }
    }

    void CreateSlot(InventoryItem newItem)
    {
        GameObject tempSlot = Instantiate(blankInventorySlot, inventoryPanel.transform.position, Quaternion.identity);
        tempSlot.transform.SetParent(inventoryPanel.transform);
        tempSlot.transform.localScale = new Vector3(1f, 1f, 1f);
        InventorySlot newSlot = tempSlot.GetComponent<InventorySlot>();
        if (newSlot)
            newSlot.Setup(newItem, this);
    }


    public void SetupDescriptionAndButton(string newName, string newDescriptionString, bool isButtonUsable, InventoryItem newItem)
    {
        currentItem = newItem;
        itemNameText.text = newName;
        itemDescriptionText.text = newDescriptionString;
        useButton.SetActive(isButtonUsable);
    }

    public void UseButtonPressed()
    {
        if (currentItem)
        {
            currentItem.Use();
        }
    }

    public void AddToInventory()
    {
        if (characterInventory)
        {
            characterInventory.playerInventory.Add(itemToAdd);
            CreateSlot(itemToAdd);
        }
    }
}
