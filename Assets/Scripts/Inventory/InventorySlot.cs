﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    [SerializeField] private Text itemCountText;
    [SerializeField] private Image itemImage;

    public InventoryItem _item;
    public InventoryManager _manager;
    private Button _button;

    public void Setup(InventoryItem newItem, InventoryManager newManager)
    {
        _item = newItem;
        _manager = newManager;

        if (_item)
        {
            itemImage.sprite = _item.itemImage;
            itemCountText.text = _item.numberHeld.ToString();
        }
    }

    private void Awake()
    {
        _button = this.gameObject.GetComponent<Button>();
        _button.onClick.AddListener(ClickedOn);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickedOn()
    {
        if (_item)
        {
            _manager.SetupDescriptionAndButton(_item.itemName, _item.itemDescription, _item.usable, _item);
        }
    }
}
