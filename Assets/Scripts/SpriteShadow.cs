﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteShadow : MonoBehaviour {

	public Vector2 offset = new Vector2 (-3, -3);
    public float maxOffsetRange;
    public float minOffsetRange;

	private SpriteRenderer sprRndCaster;
	private SpriteRenderer sprRndShadow;

	private Transform transCaster;
	private Transform transShadow;

	public Material shadowMat;
	public Color shadowColor;

    private bool shadowCast;

    private DayNightController _dayNightController;

    private float dayStartTime = 0.2f;
    private float dayEndTime = 0.85f;
    private float normalizedDayTime = 0.0f;

    private void Awake()
    {
        _dayNightController = FindObjectOfType<DayNightController>();
    }

    // Use this for initialization
    void Start () {
		transCaster = this.transform;
		transShadow = new GameObject ().transform;
		transShadow.parent = transCaster;
		transShadow.gameObject.name = "Shadow";
		transShadow.localRotation = Quaternion.identity;

		sprRndCaster = GetComponent<SpriteRenderer> ();
		sprRndShadow = transShadow.gameObject.AddComponent<SpriteRenderer> ();

		sprRndShadow.material = shadowMat;
		sprRndShadow.color = shadowColor;
		sprRndShadow.sortingLayerName = sprRndCaster.sortingLayerName;
		sprRndShadow.sortingOrder = sprRndCaster.sortingOrder - 1;
	}

    private void Update()
    {
        if (_dayNightController)
        {
            if (_dayNightController.CurrentTimeOfDay >= dayStartTime && _dayNightController.CurrentTimeOfDay <= dayEndTime)
            {
                shadowCast = true;
                normalizedDayTime = normalize(_dayNightController.CurrentTimeOfDay, dayStartTime, dayEndTime);
            }
            else
            {
                shadowCast = false;
            }
        }
        else
        {
            shadowCast = true;
        }

        transShadow.gameObject.SetActive(shadowCast);
    }

    // Update is called once per frame
    void LateUpdate () {
        float scaledXOffset = revertNormalize(normalizedDayTime, minOffsetRange, maxOffsetRange);
		transShadow.position = new Vector2 (transCaster.position.x + scaledXOffset,
			transCaster.position.y + offset.y);

		sprRndShadow.sprite = sprRndCaster.sprite;
	}

    float normalize(float input, float min, float max)
    {
        float average = (min + max) / 2;
        float range = (max - min) / 2;
        float normalized_x = (input - average) / range;
        return normalized_x;
    }

    float revertNormalize(float input, float min, float max)
    {
        float average = (min + max) / 2;
        float range = (max - min) / 2;
        float denormalized_x = (input * range) + average;
        return denormalized_x;
    }
}
