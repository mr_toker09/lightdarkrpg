﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    walk,
    attack,
    interact,
    idle
}

public class PlayerController : MonoBehaviour
{
    public PlayerState currentState;
    private float speed;
    public float moveSpeed;
    public VectorValue startingPosition;

    private Vector3 change;
    private BoxCollider2D _collider;
    private Rigidbody2D _rigidbody;
    private Animator _anim;

    void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }


    // Start is called before the first frame update
    void Start()
    {
        currentState = PlayerState.idle;
        _anim.SetFloat("moveX", 0);
        _anim.SetFloat("moveY", -1);
        speed = moveSpeed;
        transform.position = startingPosition.initialValue;
    }

    // Update is called once per frame
    void Update()
    {
        change = Vector3.zero;
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");

        if (currentState == PlayerState.walk || currentState == PlayerState.idle)
        {
            UpdateAnimationAndMove();
        }

        if (Input.GetButtonDown("Inventory"))
        {
            if (GameManager.instance.currentGameState != GameState.Pause)
            {
                GameManager.instance.currentGameState = GameState.Pause;
            }
            else
            {
                GameManager.instance.currentGameState = GameState.InGame;
            }

        }
    }

    void UpdateAnimationAndMove()
    {
        if (change != Vector3.zero)
        {
            MoveCharacter();
            _anim.SetFloat("moveX", change.x);
            _anim.SetFloat("moveY", change.y);
            _anim.SetBool("moving", true);
        }
        else
        {
            _anim.SetBool("moving", false);
        }
    }

    void MoveCharacter()
    {
        change.Normalize();
        _rigidbody.MovePosition(transform.position + change * speed * Time.deltaTime);
    }
}
