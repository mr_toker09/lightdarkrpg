﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomMove : MonoBehaviour
{
    private CameraMovement _camera;

    public Vector2 cameraChange;
    public Vector3 playerChange;

    public bool needText;
    public string placeName;
    public Text placeNameText;

    private void Awake()
    {
        _camera = Camera.main.GetComponent<CameraMovement>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _camera.minPosition += cameraChange;
            _camera.maxPosition += cameraChange;
            other.transform.position += playerChange;
            if (needText)
            {
                StartCoroutine(PlaceNameCo());
            }
        }
    }

    private IEnumerator PlaceNameCo()
    {
        placeNameText.gameObject.SetActive(true);
        placeNameText.text = placeName;
        yield return new WaitForSeconds(3f);
        placeNameText.gameObject.SetActive(false);

    }
}
